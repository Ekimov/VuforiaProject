﻿using UnityEngine;
using System.Collections;

public class Wave : MonoBehaviour {

    public float speed;
	void Start () {
        transform.eulerAngles = new Vector3(Random.Range(-180,180), Random.Range(-180, 180), Random.Range(-180, 180));
	}
	
	
	void Update () {
        transform.localScale = speed * Time.deltaTime* new Vector3 (1,0,1);
	}
}
